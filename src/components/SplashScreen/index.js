import React, { Component } from 'react'
import { Spinner } from 'reactstrap'

import tumbasinLogo from '../../assets/icons/tumbasin-logo.png'

class SplashScreen extends Component {
  render () {
    return (
      <div className='splash-screen'>
        <img src={tumbasinLogo} alt='Tumbasin Logo' />
        <Spinner />
      </div>
    )
  }
}

export default SplashScreen
