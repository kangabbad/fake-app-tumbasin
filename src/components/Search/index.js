import React from 'react'
import { InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap'
import { IoIosSearch } from 'react-icons/io'

const SearchBar = () => {
  return (
    <InputGroup className='header__search__container'>
      <InputGroupAddon addonType='prepend'>
        <InputGroupText className=''>
          <IoIosSearch
            color='#707585'
            size={22}
          />
        </InputGroupText>
      </InputGroupAddon>
      <Input
        placeholder='Cari sayur, bumbu dapur, lauk pauk...'
        className='header__search__input'
      />
    </InputGroup>
  )
}

export default SearchBar
