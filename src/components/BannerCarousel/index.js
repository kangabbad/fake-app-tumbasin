import React, { Component } from 'react'
import PropTypes from 'prop-types'
import AliceCarousel from 'react-alice-carousel'

class BannerCarousel extends Component {
  render () {
    return (
      <div className='banner__container'>
        <AliceCarousel
          mouseTrackingEnabled
          buttonsDisabled
          infinite
          autoPlay
          autoPlayInterval={3000}
        >
          {this.renderBannerItem()}
        </AliceCarousel>
      </div>
    )
  }

  renderBannerItem = () => {
    const { banners, handleOnDragStart } = this.props
    return banners.map((item, index) => {
      const alt = `Banner ${index + 1}`
      return (
        <img
          key={index}
          src={item.guid.rendered}
          alt={alt}
          onDragStart={handleOnDragStart}
        />
      )
    })
  }
}

BannerCarousel.propTypes = {
  banners: PropTypes.array,
  handleOnDragStart: PropTypes.func
}

export default BannerCarousel
