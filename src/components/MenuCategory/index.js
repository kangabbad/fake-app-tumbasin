import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Col, Button } from 'reactstrap'

class MenuCategory extends Component {
  render () {
    const { categories } = this.props
    return categories.map((item, index) => {
      return (
        <Col
          key={index}
          className='
            d-flex
            flex-column
            justify-content-center
            align-items-center
            menu__button__wrapper
          '
        >
          <Button
            color='link'
            className='menu__button__item'
          >
            <img src={item.image.src} alt={`Button ${item.name}`} />
          </Button>

          <p className='menu__button__title'>
            {item.name}
          </p>
        </Col>
      )
    })
  }
}

MenuCategory.propTypes = {
  categories: PropTypes.array
}

export default MenuCategory
