import React from 'react'
import { Col, Row } from 'reactstrap'
import Skeleton from 'react-loading-skeleton'

const MenuSkeleton = () => {
  return Array(8)
    .fill()
    .map((_item, index) => (
      <Col
        key={index}
        xs='3'
        md='1'
        className='
          d-flex
          flex-column
          justify-content-center
          align-items-center
          menu__button__wrapper
        '
      >
        <Skeleton height={64} width={64} />
        <p className='menu__button__title'>
          <Skeleton height={10} width={50} />
        </p>
      </Col>
    ))
}

const BannerSkeleton = () => {
  return (
    <div className='banner__container'>
      <Skeleton height={140} width='100%' />
    </div>
  )
}

const ProductSkeleton = () => {
  return (
    <Row className='product__list__item' style={{ minHeight: 100 }}>
      <Skeleton height={70} width={70} />

      <div className='product__list__item__info'>
        <Skeleton height={12} width={175} />

        <div style={{ marginTop: 5 }}>
          <Skeleton height={12} width={125} />
        </div>

        <div style={{ marginTop: 5 }}>
          <Skeleton height={12} width={75} />
        </div>
      </div>
    </Row>
  )
}

export { MenuSkeleton, BannerSkeleton, ProductSkeleton }
