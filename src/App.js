import React, { Component, lazy, Suspense } from 'react'
import { Row } from 'reactstrap'
import Axios from 'axios'

import ErrorBoundary from './components/ErrorBoundary'
import SplashScreen from './components/SplashScreen'
import MenuCategory from './components/MenuCategory'
import BannerCarousel from './components/BannerCarousel'
import ProductItem from './components/ProductItem'
import { MenuSkeleton, BannerSkeleton, ProductSkeleton } from './components/SkeletonComponent'

const Header = lazy(() => import('./components/Header'))
const Footer = lazy(() => import('./components/Footer'))

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      categories: JSON.parse(localStorage.getItem('App_categories')) || [],
      banners: [],
      products: JSON.parse(localStorage.getItem('App_products')) || [],
      cart: JSON.parse(localStorage.getItem('App_cart')) || [],
      cartInfo: JSON.parse(localStorage.getItem('App_cart_info')) || {
        totalItem: 0,
        totalPrice: 0
      }
    }
  }

  componentDidMount () {
    this.getCategories()
    this.getBanner()
    this.getProducts()
  }

  getCategories = () => {
    Axios.get('https://api.tumbasin.id/v1/products/categories')
      .then(res => {
        const { categories } = this.state
        const localCategories = JSON.parse(localStorage.getItem('App_categories'))

        if (localCategories === null) {
          if (res.data !== categories) {
            this.setState({ categories: res.data })
            localStorage.setItem('App_categories', JSON.stringify(res.data))
          }
        }
      })
      .catch(err => console.log(err))
  }

  getBanner = () => {
    Axios.get('https://dashboard.tumbasin.id/wp-json/wp/v2/media?media_category[]=39')
      .then(res => {
        // const { banners } = this.state
        // const localBanners = JSON.parse(localStorage.getItem('App_banners'))

        // if (localBanners === null) {
        //   if (res.data !== banners) {
        //     this.setState({ banners: res.data })
        //     localStorage.setItem('App_banners', JSON.stringify(res.data))
        //   }
        // }

        this.setState({ banners: res.data })
      })
      .catch(err => console.log(err))
  }

  getProducts = () => {
    Axios.get('https://api.tumbasin.id/v1/products?vendor=1000&featured=true')
      .then(res => {
        const newProducts = []

        res.data.forEach((item, index) => {
          const newData = {}

          newData.image = item.images[0].src
          newData.name = item.name
          newData.price = item.price
          newData.pcs = item.meta_data[0].value
          newData.count = 0

          newProducts[index] = newData
        })

        const { products } = this.state
        const localProducts = JSON.parse(localStorage.getItem('App_products'))

        if (localProducts === null) {
          if (res.data !== products) {
            this.setState({ products: newProducts })
            localStorage.setItem('App_products', JSON.stringify(newProducts))
          }
        }
      })
      .catch(err => console.log(err))
  }

  render () {
    return (
      <ErrorBoundary>
        <Suspense fallback={this.renderLoading()}>
          {this.renderHeader()}
          {this.renderMenu()}
          {this.renderBannerCarousel()}
          {this.renderProduct()}
          {this.renderFooter()}
        </Suspense>
      </ErrorBoundary>
    )
  }

  renderLoading = () => {
    return <SplashScreen />
  }

  renderHeader = () => {
    return <Header />
  }

  renderMenu = () => {
    return (
      <div className='menu'>
        <h6 className='menu__title'>
          Telusuri Jenis Produk
        </h6>

        <Row
          className='
            menu__button__container
            row-cols-5
          '
        >
          {this.renderMenuItem()}
        </Row>
      </div>
    )
  }

  renderMenuItem = () => {
    const { categories } = this.state
    if (categories.length) {
      return <MenuCategory categories={categories} />
    } else {
      return <MenuSkeleton />
    }
  }

  renderBannerCarousel = () => {
    const handleOnDragStart = (e) => e.preventDefault()
    const { banners } = this.state
    if (banners.length) {
      return <BannerCarousel banners={banners} handleOnDragStart={handleOnDragStart} />
    } else {
      return <BannerSkeleton />
    }
  }

  renderProduct = () => {
    const { cartInfo } = this.state
    const style = cartInfo.totalItem
      ? { paddingBottom: 140 }
      : {}

    return (
      <div className='product' style={style}>
        {this.renderProductHeader()}
        {this.renderProductList()}
      </div>
    )
  }

  renderProductHeader = () => {
    return (
      <Row noGutters className='justify-content-between'>
        <h6 className='product__title'>
          Produk Bestseller
        </h6>

        <a href='!#' className='product__button--more'>
          Lihat Semua
        </a>
      </Row>
    )
  }

  renderProductList = () => {
    const { products } = this.state
    if (products.length) {
      return products.map((item, index) => {
        return (
          <ProductItem
            key={index}
            src={item.image}
            name={item.name}
            price={item.price}
            pcs={item.pcs}
            count={item.count}
            onAddToCart={() => this.onAddToCart(index)}
            onReduceToCart={() => this.onReduceToCart(index)}
          />
        )
      })
    } else {
      return Array(5)
        .fill()
        .map((_item, index) => <ProductSkeleton key={index} />)
    }
  }

  onAddToCart = (index) => {
    const { products } = this.state
    products[index].count++
    this.setState({ products }, () => {
      this.buildCartCalculation()
    })
  }

  onReduceToCart = (index) => {
    const { products } = this.state
    products[index].count--
    this.setState({ products }, () => {
      this.buildCartCalculation()
    })
  }

  buildCartCalculation = () => {
    const { products, cart } = this.state
    const selectedItem = []
    const selectedPrice = []
    const newCart = []
    const newProducts = [...products]

    products.forEach((item, index) => {
      if (item.count > 0) {
        newCart.push(item)
        newProducts[index] = item
        selectedItem[index] = item.count
        selectedPrice[index] = item.price * item.count
      }
    })

    const totalItem = selectedItem.length ? selectedItem.reduce((a, b) => a + b) : 0
    const totalPrice = selectedPrice.length ? selectedPrice.reduce((a, b) => a + b) : 0
    const cartInfo = { totalItem, totalPrice }

    this.setState({ cart, cartInfo })

    localStorage.setItem('App_products', JSON.stringify(newProducts))
    localStorage.setItem('App_cart', JSON.stringify(newCart))
    localStorage.setItem('App_cart_info', JSON.stringify(cartInfo))
  }

  renderFooter = () => {
    const { cartInfo } = this.state
    return <Footer cartInfo={cartInfo} />
  }
}

export default App
