import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Row, Button } from 'reactstrap'
import { FaPlus, FaMinus } from 'react-icons/fa'

import { toIDR } from '../../utils'

class ProductItem extends Component {
  render () {
    return (
      <Row className='product__list__item'>
        {this.renderImage()}
        {this.renderInfo()}
      </Row>
    )
  }

  renderImage = () => {
    const { src, name } = this.props
    return (
      <img
        src={src}
        alt={name}
      />
    )
  }

  renderInfo = () => {
    return (
      <div className='d-flex flex-column justify-content-between product__list__item__info'>
        {this.renderProductName()}
        {this.renderProductAction()}
      </div>
    )
  }

  renderProductName = () => {
    const { name } = this.props
    return (
      <p className='product__list__item__info__name'>
        {name}
      </p>
    )
  }

  renderProductAction = () => {
    return (
      <Row noGutters className='justify-content-between align-items-center'>
        {this.renderProductPrice()}
        {this.renderCountBtn()}
      </Row>
    )
  }

  renderProductPrice = () => {
    const { price, pcs } = this.props
    return (
      <p className='product__list__item__info__price'>
        <span className='product__list__item__info__price--currency'>
          {toIDR(price)}
        </span>
        <span className='product__list__item__info__price--pcs'>
          /{pcs}
        </span>
      </p>
    )
  }

  renderCountBtn = () => {
    const { count } = this.props
    if (count > 0) {
      return this.renderStepper()
    }

    return this.renderAddBtn()
  }

  renderAddBtn = () => {
    const { onAddToCart } = this.props
    return (
      <Button
        className='product__list__item__info--add'
        onClick={onAddToCart}
      >
        Tambahkan
      </Button>
    )
  }

  renderStepper = () => {
    const { count, onAddToCart, onReduceToCart } = this.props
    return (
      <Row
        noGutters
        className='align-items-center'
        style={{ position: 'relative' }}
      >
        <Button
          className='product__list__item__info--minus'
          onClick={onReduceToCart}
        >
          <FaMinus
            color='#c4c4c4'
            size={14}
            style={{ paddingBottom: 3 }}
          />
        </Button>

        <div className='product__list__item__info__count'>
          {count}
        </div>

        <Button
          className='product__list__item__info--plus'
          onClick={onAddToCart}
        >
          <FaPlus
            color='#ffffff'
            size={14}
            style={{ paddingBottom: 3 }}
          />
        </Button>
      </Row>
    )
  }
}

ProductItem.propTypes = {
  src: PropTypes.string,
  name: PropTypes.string,
  count: PropTypes.number,
  price: PropTypes.string,
  pcs: PropTypes.string,
  onAddToCart: PropTypes.func,
  onReduceToCart: PropTypes.func
}

export default ProductItem
