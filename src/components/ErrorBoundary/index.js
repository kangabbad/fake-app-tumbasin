import React from 'react'
import PropTypes from 'prop-types'

class ErrorBoundary extends React.Component {
  constructor (props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError (_error) {
    return { hasError: true }
  }

  render () {
    if (this.state.hasError) {
      return this.renderError()
    } else {
      return this.props.children
    }
  }

  renderError = () => {
    return (
      <div className='error-boundary'>
        <p>Loading failed! Please reload.</p>
      </div>
    )
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.any
}

export default ErrorBoundary
