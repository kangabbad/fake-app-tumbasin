import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Button } from 'reactstrap'
import { MdShoppingCart } from 'react-icons/md'

import { toIDR } from '../../utils'

import home from '../../assets/icons/ic_home.svg'
import transcation from '../../assets/icons/ic_transaction.svg'
import help from '../../assets/icons/ic_help.svg'
import user from '../../assets/icons/ic_user.svg'

class Footer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tabs: [
        {
          icon: home,
          label: 'Belanja',
          active: true
        },
        {
          icon: transcation,
          label: 'Transaksi',
          active: false
        },
        {
          icon: help,
          label: 'Bantuan',
          active: false
        },
        {
          icon: user,
          label: 'Profile',
          active: false
        }
      ],
      show: false
    }
  }

  componentDidMount () {
    window.addEventListener('click', this.handleClick)
    window.addEventListener('load', this.handleClick)
  }

  componentWillUnmount () {
    window.removeEventListener('click', this.handleClick)
    window.removeEventListener('load', this.handleClick)
  }

  handleClick = () => {
    const { cartInfo } = this.props
    if (cartInfo.totalItem > 0) {
      this.setState({ show: true })
    } else {
      this.setState({ show: false })
    }
  }

  render () {
    return (
      <div className='footer'>
        {this.renderTab()}
        {this.renderFloatingCart()}
      </div>
    )
  }

  renderTab = () => {
    const { tabs } = this.state
    return (
      <Row noGutters className='footer__tab'>
        {tabs.map((item, index) => {
          const spanClassName = item.active ? 'footer__tab__button--active' : 'footer__tab__button--inactive'
          return (
            <Col key={index} xs='3'>
              <Button
                color='link'
                className='
                  d-flex
                  flex-column
                  justify-content-center
                  align-items-center
                  footer__tab__button
                '
              >
                <img src={item.icon} alt={`Tab ${item.label}`} />
                <span className={spanClassName}>
                  {item.label}
                </span>
              </Button>
            </Col>
          )
        })}
      </Row>
    )
  }

  renderFloatingCart = () => {
    const { cartInfo } = this.props
    const { show } = this.state
    const className = show
      ? 'footer__floating-cart__container effect-slide--down--reverse'
      : 'footer__floating-cart__container effect-slide--up--reverse'

    return (
      <div className={className}>
        <Button
          color='link'
          className='w-100'
          onClick={() => {}}
        >
          <Row
            noGutters
            className='
              footer__floating-cart__wrapper
              justify-content-between
              align-items-center
            '
          >
            <div>
              <p className='footer__floating-cart--subtotal text-left'>
                {cartInfo.totalItem} item | {toIDR(cartInfo.totalPrice)}
              </p>
              <p className='footer__floating-cart--loc text-left'>
                Pasar Karangayu
              </p>
            </div>

            <MdShoppingCart
              color='#ffffff'
              size={22}
            />
          </Row>
        </Button>
      </div>
    )
  }
}

Footer.propTypes = {
  cartInfo: PropTypes.object
}

Footer.defaultProps = {
  cartInfo: {
    totalItem: 0,
    totalPrice: 0
  }
}

export default Footer
