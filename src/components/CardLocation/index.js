import React from 'react'
import { Row, Button } from 'reactstrap'
import { FaStore } from 'react-icons/fa'

const CardLocation = () => {
  return (
    <div className='header__card-location'>
      <p className='header__card-location__title'>
        Kamu Belanja di:
      </p>

      <Row
        noGutters
        className='
          header__card-location__store
          justify-content-between
          align-items-center
        '
      >
        <Row noGutters>
          <FaStore
            color='#87CAFE'
            size={20}
          />

          <p>
            Pasar Karangayu
            {/* <span>
              1,3 km
            </span> */}
          </p>
        </Row>

        <Button>
          Ganti
        </Button>
      </Row>

      {/* <p className='header__card-location__address'>
        <span className='header__card-location__address--label'>
          Lokasi Anda:
        </span>
        Jl. Pasar Karang Ayu, Kecamatan Semarang Bar, Kota Semarang, Jawa Tengah
      </p> */}
    </div>
  )
}

export default CardLocation
