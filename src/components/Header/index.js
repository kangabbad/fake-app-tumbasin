import React, { Component } from 'react'
import { Row } from 'reactstrap'
import { FaStore } from 'react-icons/fa'
import { FiChevronDown } from 'react-icons/fi'

import SearchBar from '../Search'
import CardLocation from '../CardLocation'
import { IoIosSearch } from 'react-icons/io'

class Header extends Component {
  constructor (props) {
    super(props)
    this.state = {
      show: true,
      scrollPos: 0
    }
  }

  componentDidMount () {
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount () {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll = () => {
    this.setState({
      scrollPos: document.body.getBoundingClientRect().top,
      show: document.body.getBoundingClientRect().top > -150
    })
  }

  render () {
    return (
      <div>
        {this.renderHeaderBoard()}
        {this.renderHeaderNavbar()}
      </div>
    )
  }

  renderHeaderBoard = () => {
    const { show } = this.state
    const className = show
      ? 'header effect-slide--up'
      : 'header effect-slide--down'

    return (
      <div className={className}>
        <SearchBar />
        <CardLocation />
      </div>
    )
  }

  renderHeaderNavbar = () => {
    const { show } = this.state
    const className = show
      ? 'header__navbar justify-content-between align-items-center effect-slide--down'
      : 'header__navbar justify-content-between align-items-center effect-slide--up'

    return (
      <Row
        noGutters
        className={className}
      >
        {this.renderPickLocation()}
        {this.renderSearch()}
      </Row>
    )
  }

  renderPickLocation = () => {
    return (
      <Row
        noGutters
        className='header__navbar__location align-items-center'
      >
        <FaStore
          color='#87CAFE'
          size={25}
        />

        <div className='header__navbar__location__info'>
          <h6>
            Pasar Karangayu
          </h6>
          <p>
            1,3 kilometer dari lokasi anda
          </p>
        </div>

        <FiChevronDown
          color='#4E5356'
          size={25}
        />
      </Row>
    )
  }

  renderSearch = () => {
    return (
      <IoIosSearch
        color='#707585'
        size={25}
        style={{ margin: '15px 20px' }}
      />
    )
  }
}

export default Header
